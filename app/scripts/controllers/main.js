'use strict';

angular.module('playbuzzAppApp')
  .controller('MainCtrl', function ($scope, $http, $q, Sources, $compile, $rootScope) {
    $scope.sourceInfo = null;
    $scope.selectedQueryResult = null;

    $scope.onUrlChanged = function (url) {
      $scope.sourceInfo = Sources.parseSourceUrl(url);
    };

    $scope.onResultSelected = function (result) {
      $scope.selectedQueryResult = result;
    };

    $scope.execQuery = function (query) {
      return Sources.querySources(query);
    };

    /* Simulation of adding and removing sources on runtime */
    /* ---------------------------------------------------- */
    function hookYoutubeSource() {
      var name = 'youtube',
        logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfYAAADSCAYAAAC4oeIoAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AgfEyUEE2S0/QAAG9xJREFUeNrt3V10E+edx/EfNgSW1EKkpM1GCRawG6S8NMp2MbubthaB62BnLxvAXG4gG7gC0guacxbIXiVNyDW209tgc7uBIi56zuK2WyUksXB4UTbRpoc0wcg1BwLUe6FRYjvYlmaemXlm9P2c49MGy/PyaKTf/J/nmZlFAgDM8OmPMmlJ6Tjsy8Pvlwq8o61lEU0AIKCwzM/z66SkXAOL6W5ilTlnuXCvKGm8wdeWJX3SwGvK8/3+4fdLZZqdYAfgb/je7d+enCM0CVMEfZJRlHStgROIljlpINiBmPnfH2W+CddFM0O5UzO7lxutkoE4GndOCqb/93v1/5iaeXJQXP1+aTwqO0awA9EM77wTyp3TqmRCGgjmZKCs2rBD0Ql9q3oCCHYgGkHeo9r4cp4AB6wM/IKkM5KGww56gh2wO8y3SuoR49ZAlBQlDYQV8gQ7YFeYJyXtkbRDMbncCmhx/ZIGVgd42SHBDtgQ6E9kklqkPZJeojoHYqmgKb2y+pz/AU+wAyH75InMHkkHFxHoQOxNScOS9nae86+LnmAHwgv0nKRjYjIc0GrGJb3Sea70OsEOxKtKf42WAFpaQVJv5zmz18gT7ECwgZ50qvQeWgOAU71v6jxXKhLsQDRD/bToegfwXTs7z5X6TSyojbYEAgn1nKTLhDqAORxzhuio2AHblWuhflrMegewsP70udJOKnaAUAcQD33lJzKeJtZSsQP+hXpS0h/FHeQANG9n2uWYOxU74J/ThDoAl445PX4EO2BJtf5LMVEOgDdDTs8fwQ6EHOo5SQdpCQAepd18lzDGDhh2+YnMadWemw4AJmxa08TDY6jYAbOh3keoAzCsqaqdYAdC/AACQAPyTtFAsAMhVOtpWgJAmEUDY+yAqWB/PHOZYAfgo941H5SGqdiBYEK9h1AH4LOXqNiBgFx6nJnwAAKxZu0HpTIVO+BvqKcJdQC2VO0EOxDABw0ADOkj2AH/9dAEAAKSvFSb00OwA3649HgmJybNAQjW1vl+uZj2AdybknbQCgAC1iNpJxU74N8HDACClLw4T3c8wQ64dJFueADh2TpfsF+VNGX4J8gqpseH7Z/imEED8jQBANu+fxZLGlYD0+eb1O0sNwjdPixzmGMGXs6Y3Vh7brRlG/LSE9mWP5hMvf+0ZctIX3w8k1v3Qal4t4r9TMQrGT/WdYJjBlTsAGw2Ncd3UJtP1WlOUjKA/Uo666JiR6AuPJ7JM/Zj9Auq5X9oS35cHDPdcwX7uE9BFsQ4ux/rKDhtAlCtA4jc91B9VrwfXc/dAeyUH+ugGx62HN8AMJ/khdrVOXcNdj8q9iAqGj/WQTc8FjblyxAQAHj+Lqrfea7eHW+yazvt/JR92p368k0q+ri9iIkLj2XSkpJcFGn0ywm0Jdx5cq5gl2pd0KbHrHskve7Tzvgxvk43PBr53qRaJ4toS9hizq54yZ8uaD/HIbl+HdZ8kAAgJPn5gn1cta5oX1do8bLLPuw/4ulJmgCALT6uDQ/eNdglacDw+vy6ztyP6+Sp1tHMcQ0Atpg32KNyPbsfPQGMryPM4w8AfAn2ssx3R/sxFr7V8PLGVbsxDQAAkQ72xXNUria7z/2obkwvk254NGTssUyOWcfm0aa0JTxZMV/F7lfImeyO9+NEgW54NIrxdQC2yS0U7H7cpMVkd/xWHxqFih0EO4BYaAso6PKWLotQh6czYwCw7Xtp8RwvGpC0x/BKk/L+1DQ/Lp+jGx5NYASTdqUtYZ1kIxV7UeYfXWpinD3vQ4NQsaMZ3JwGgHXOP7Y+uVDFXg+8PoPr7ZbU73EZpsfXC+LZ62iuFvJtjH3s8YzxZT7yQcn6bQT1OozIOZk2Z8Uume+izluyDD/3EfHH5DkAVluoYh83+EWWlrfHuNb/3iS64dFsOZRr8f0H7xHs9E0+ti3wwoLhFXsZZzddrfPsdQBAywW76a7q7pD+Noh9Q8ydf3Q93fAArLd4gd8PSzpmcH1eqm7TD5OhGx5NmeIadnp5eY9gr85GK/ZxwwHo9jp0049pLYtnrwMA4iPdaLBL0hnDK3dTeecNbwPVOtyemAKA1RoJdtMh2B3Q38yH8XW4we1kAcSiYi/LbLe1m+rb5Pg6z14HALR0sEu1e8eb1ExQ5w2vm254AEBsLW7wdaYr3O4mAtZ0sNMND1emzA8JRbENwHsEyzVasZu+mUszYW36y5SKHQAQO6OPrs83U7HXA9HUo1wbfYxr0nDFHlSop519zKl2bWF6gdefcdqiKMb/AYRgSSql5Ru6tCSV0rL1WbUlOr753c1SSbcqFV3/3YhulEYjuX/LMlm1dXRoeVfXnK+5VanoVqWim6WS7kxUI/teNhPspp/R3qOFn/aWN7y/fnbD5yTtcPYr3eTfzt7PgrOt/Qrv6XMme/QWxWA76ieaQGy0dyS0oqdXK3p6tTQz95P7lm/omhF+V98e1LXhIavDb1kmq+9t3qzlG7pmbH+j6icyf/nNKV0fGYlU0Df7RXfV4Jdbv6SdC7zmNcMnEyt9CMo+SQdl/gE103sZfhVCJU+wzzL66PrIDV9mPjT72NbSYxkrt83LdsVtOxrZhvaOhFZu3677tu1QW0eHq/XcqlR05T+PaOLUSetOVFZu264lqZSx5f51YkITp07q6tuDtvdY9GY/Oj/c1uQfmezKzht6TaMKhkO9R9Jl1W65m/bxjeqRdNr54TpqAJ7ct22H1r17Sqte2O061KVa133qjaNatWt36PvU1pHQql27te7dU/rB/gNGQ722/A6t6OlV+p0hre4fNL58g3JS45Pn6kx2ZacXCMS04SAzte1JSUPOTzrANywv6Y+SfslXUzimIvhjcxvYsl1x2465lt/WkdDq/kH9YP8BT4E+26oXdutvDx0J7Rj/3uYtWvPOkOcTlUYt39Cldf91Sqt27bb2M++mYjdd9QZRrZvqbcg54dqj8Bx0toHxXgANWZbJat27p1yNNTeiPk4fpPaOhB48dEQPvXE0lAp61Qu7teadYS3LZK17v9tc/E3B4Pq7Xf6uWSYu18up1h2etuB9y6k2DEDXfEA+yq7PU7JTskexZF+xtVdr3hlSu8/V7IOHjmjZ+mwgx3X792q9D0GfTHz3hCmj1f2DtRMmiz7zboLdZHd8PqCK3es210Pdpio5KcbdASxQST94+Ehg6/vhgQOB9D6sOT6kZZmMFW3c3tGhTgtOMqYXxG6CPYjHuOYMV8ZetjltYajPDve0ACDEUJdq488dm7f4GKIJrR6wc/Lag4eP2BLuroLd9ENUenyu1sty/xCb+kQ5m8ez69sIAKGFet1927f7E1ZOqLcHMEHOrR8eeFlLLRhzb3P5dya747sb/LcwqvWDikZXd061a/7hkykxK15iiD0K27G8qyu0UK9X7UszWePH8sNHj1rT/T53j0KHOgcG1daRiNSseBNh2Uh1brJiP+Fhu/YoOvbI/JUE8OeYBHzz0Jtvhb4NScNd0vfv2u3bjH4/wj3MEysvwV6W2We098yqPk11fXsZNohiBXyQrzWgtdnQVd2xebOxZS1JpXS/BTfBaXb/k+GMt6e9BLuXSvhuuucIea/c9iz0KZqzzfPOtgNAaJakUsYmuKVCrn699DK0dyQiF+x+dcebHF93e/LxUoQ/Uzv4WgEQtnsNdJ0v7+qKTBf83U5u/JpIuBAvwW7yGe3Tu9/zIVfsOUX72vC8uLbdOCbPmW8HW7Yrjtthg+VdXZ6Pkah1wc+W7OmN1OQ5P6r2HtnRDf9SDD5TVO0AQuX1VqtLUikjVX/YVXsYY+2LPf79GZmbOd4ts/ehd9sN79d94IuqTeS75vz3Ch+r6z5Je/lqAVA3ceqUJn83MuOxo8syWXVs3uxLgHq9NO3728zXJ7cqFVWddvjrtOert3Uk9DeZjJI9vcZvfpPYvEXjw8HeasRrsNcfCmNiFnvecLC77YY3fTOaohOyhXn2+zXDAV+/o19RMKWTJkDU3JmY0FeDA/pycFB3pgVZ3eTIiL4cHNC9XV16+M23jM+oX5bJun5+ecLgzPo7ExP64q2j+nJwYJ4Tn5O68tZRfX/7Dmfim5m26Ni8We0dibu2v1/aDCzDVHd82mC4FVyeJJiu1vslPaX5L7krOK/pN7zuHsGkNE2AKJn83YguPtejK28dXTBUJkdG9OmLu4xvQ3vCXTguy2SNVs6fvrhr3lCf7svBAeNtYfLyv6CC/YyFx7TbbniTM/LLaq47fK/MTUY0vS8tj8lz5tvBlu2K43ZceeuoLu/Yrq8rlYbX/5eREeNdxss3uJtAt7zL3NDA+PCQ/jIy0tT6TbfFvQYmEgY5ec5kxW6S223KG9yGnU32Gow7f2NKXiDZSfaW247Kywd05ehRV9vw54FBK44Zk2P+btviytGj5oI9yMe6Ggr2ccvC3e1leCbHuMtyd8e7guGqncvegBZzdch9pXmjNKpblYrZQHNZ4Zpwo1TS1y735+tKRTdKJSPbsSSVCuxmNecy63NthpZ1wqLj2u22pC3oMTDdA5IWADRhcmQk1PXfk0oZm7jmdV9MtsWybGAPsEmaCvaCRcel22A0Wd16mXdg8iSJih1Ac5Xq/5mr2NsTzVepyww+9vRrj70PX5vsvegK7pp8U8Felh2XVnnZDpOXMxU97oMpXKIFoCk3RkvGluXmWnaTla3bS+1M/f2Mk5wA7xu/2OCyBiyoEL10Y6cNn2DYEOxpwYgpmsDqdphiO4xtw23D11s3uz1LHkwZXfeUJe/nskwmsOOjzeCyChYc0zaN9dsgSRMAiJJ7DN/5rRWZDHaTD4Vxw8uz16V4Xh7GGDuAlnWnOmHNtkRxjL0uzMvehjmMASDaTAag5zF2g/MNolqxS+F2hdMNDwBewzCiYeZLxR/g/d1tDvaCzD7IhYodAAgzhBjsYQUsoQ5fcUdZ8+1gy3bFbTui/t4oxm2xLJMN5HPvR7CH0SVONzwAwGpun3ZnQ8VeoGIHACAcfgR70A+FKSi8cX0AAGIf7FKwXeN0wwMA4HOwFwLcB7rhAQDWmwronrKLfVpuWbU70fl957Ow73YHPpC0A9tl1XbY9h6FuT02Hq9BbFObj8seCKCN6IYHACCAil0KpjuebngAwF09WWrNu+j5WbH73U1elh3PgAcAoCWC3e+KmmodAIBZFvu8/BOS9vi4bCAQzJ2zux2m2A5r3yM+OzPbIoj28LtiL8ifm8d4ffY6AACx1BbAOoYjskwAAAj2BpyIyDIBACDYqdgBAGitYAcAAAFZTBMAC2Nmr93twKx4e98jPjsz2yIOs+IBAECACHYAAAh2AABgoTLBDgBATPzD+fNlJs8BjWAGkN3twOw5e98jPjsz2yKA9iDYAb6byHVyPZbb8z/rMy35OaUrHmjMGZoAAMEOAAAIdgAAQLADAECw0wQAAMQHs+KBBjAr3u52YFa8ve/RFG1BxQ4AAFwpEOxA48o0AQAqdoBgB4BAMcYONIAxdrvbgTF2e98jxtip2AEAABU7AADm/eP5krFl/d7/e9eXqdiBxo3TBAAs9wnB/q0kTYD5bBg7X6QVAEQBXfFUY2gUM+iYPReF7WD2XMu3BcEOkOvkOrlOrsdje8YluuKBpj80AGCpIsEOuPjQAHHWsbErNvuyNJVqyfeQYAcAxNI9D3kL9qieGBDsAABr3KlOxObEIASMsc9SZp9AewLhul4apRFc6vq4dlkus+JnfmmnCSLMZcq5+UOLtwHbZfl2eN2G9o5EbNrknlTKmmPj+mgpsG2hYv+WyRnP6ZD+Nm5eowngp9W/eJlGmGV5NhPq+qtnR4wty6Yx8jsT1cDWRbB/670YBvuZCL8fPZL2xPjkDyFbuWWLHtixnYawLAxvVirGlpWI0Qz/BhQI9u8qG1xWPqS/jUsQ5SQds3C7uNwtNlVpVmtfPUJDzNE2YYb09VFzD13p6PIW7MktW4xty6TB/aJiDyfYt4b0t3EIoqQT6ty/H75oTyS09tUjak900Bh3qdZNdsV/7SrYzU6eW+khnFdu2WxsO+5U6YoPQ8Fwxemm8s47f9vKwX7acBsAM2TfHgx9HNlPq57rtaJCldxPnKuOmBtn/2Gfu+GWjo1dRoclTA4xLPR9T7D7F4QHA/qb+fbFlq74Rk5y6pW6taG+8ePzhSnnyyoqP6bZum0dG7sWXF9bIqE1rx7xNdRtaI+1rx7RmlePNL3+tkRCD/SZnXNw87OKq7aYMDiBLtHVpeSWLU1vQ+fLZidWTjqz4n38uUaw+1+159XcrO5jMju+blO1vtDwQtKp1Ps4BOHGQt2t7YmEHn17UPd7qGaj5P7nevXEieGmxss7Xz5gfOLc1y6r1K9OnjK6HetePdJUW6w1fAJ4pzphfIhhPgT7TCcML2+PFh4vrleqfZbvixd981TiPZIuKzrd78yMtzHIenvn/OJeuWWLnvrNqVh3v9/N8mxGT5wY0tpXj8wb2O0dtTkHfpz03HZ5F7nro6NGu67bEx169O1BPbBjx7yvW5pKKftr8yeAX508GWhhyg1qvtsw4zI7cavPCa9+1S4/G58W6N3O75N+vskWqFfk/dNOOPKSdihq1+1PqSizPSuK2P5bqf7F/cXxIV11vkQTG7u06rneYC/fsrB97n+uV/c/16vroyVdPXlSkx+VdGei6kyUy+r+53p9m0h4fXTUdZv8+fiQUi/uNnqMdP7igB7o266r756acfKQ2Nil5dms0cly011991Sgx8aiiB7ufm63H9Vz0Pol7fS4jNMxDDDPx81//936SLXLxo/NXmJz9u/NVb3ZXw/G7jpjL+1j+r2yQTG/2XXlvTSVUq5wKvJtcKc6od//eEMQq3rqny6c57Gtc3glBvswwNvo2xlqmclzdm6bLR0atMe3blQqrtvjRqWiL44PRb4NPh8YCOSzXg91gv3uyrKrGzus7edmLHf3CU1gxvXREo0QYyZuDfvZG0cjX63/qX8w8PUS7PGr2vcaWs41DgO/a9bWrtlvB3jvbGr2ME7cRj0fIzcrn0W6av98YEC3q9eC+KwXCfaFFVQbp47idg8bXFbYipa2MYxUdGctCR/YWrFL0ieHDgd61zZTblYq+lN/YKOi4wR745VvlC5tGpf3CXM2hWpR0iYOw/gKuyv++uioPno+fg+BsSEE71Srxi7xul2t6uK+A5F7Hz75j8O6Hdx7QbA30VBRupvFXpm93/14iOFeD3UbT6yYe2DI7Wo1tIq5Huq3q/EbDri470DoPRGmbzDz1cmTQVa/nv2pfyCoa9fr3iPYG1eQuTFrP/XLn6GDMD5J9Z4HK3tL/vnC2Dgj7OZ+roQwfnq7WtWFfQd0q1o13kY2vFe3Jqr68PntQdybfE6f95ufCX750GGjz2r3y+ToqC4fOhzqZ51gX9jrsnu8vV9mu+CnGw54X8pOpW57VVzmY2GoEns30KpGNysVffT8dk3GfGz9drWq8/+2K5QeierZEd/a9/wLu6x+7ybDG94pEOzN22lpuPsZ6vUAKwS0L0VJTykaXd0Eu8GgDaoKmxwd1fvP9sQ+1GeHTND7e2Hffl9PWGw9MZu0aHiHYG8u3G26DM7vUK8LYp9tHlMn2H326ZtvttSXbpzDvXzosO9DAPVwt+kyuLCPr3+5MEbF7sEvVZtQF2YA1cegdwa0voJqwxF+nqA8pWhdgcBNagyqnh3R5z5OjPri+JDef7an5UI96CD84viQr+/j7H26sG+/yocOh96+n/cPhH18lanYvRt2gqgQwroLzrr7A17vXpnvIg/6BMX0+wDDlZ7pqrL+5e9n13CUwr3eFn4E0JXjQ6G08+f9A3rv2Z5QJtXdrFT04fPbbDi5KBLs5s6QNjnVezmg9e101lkOaZ9NTmoL6wTFiCmpyKx48z8fGuwyrp4d0XvP9ujK8aFA28iW92q+qxDq7WIyXC/s2x/aMT45OqoPnt+mC/v2B3IlwO1qVZ++eVR/yD+ja2dHbPicv0ewm6/e1zih60cVV3SWvcaCEBx3wtjLmHvBOUEI8wTFs6cvjIV5jX+sq8r3nu3Rpx7uD149O6IPf75NHzy/LdTLvcI+81yo0rywb78+/Pk2T1cl1Nv6sgXd4fVegz/kn9GHP9/my2WUk6OjurBvv0Z+vEGfvvGmTe/4d7JnEV8nRuVUe8Z43vn/bsO8oNo15LaGR1rSQdWeM7/Qs+THnROgAcWoC/u36x6Jw+N9rbX0oZQe/vcXdd+WzVqcSCx4QvDVyVP64p3juhaB65xtbOv7tmzRD/61V/dms7Fp68WJhBIbu7RiY5fuzWabfkTwzUpF186OqHr2rK6dHdHNz6w9UVz59MWxcYI9GMlpAf/kPAE4rlpXStH5iVolm3N+0rP+vTxtn2Lnt+se6ZN0jMPcf/dms7r30YyWph6a9cX7mSY/KrXM5WtBWbGxS+2JxDchf7PymW5+VtGNSsXmcGv4JGZZKiVJWp7NzjhprD+74HZ1IkrHVPHpi2NPUbEDZoI9LekyLQEgRK8/fXHsO3dHZYwdcOHpi2NlcT07gHCduds/LqZdAHemanMG+mgJACEp3O0fqdgBw2fLABCA4k9mTZoj2AHvhmkCACE5MdcvCHbAJedsuUBLALCpsCDYAZ/OmgHAJ+WfXByb81JiJs8BHs+ap6TXaAYANlTrVOyARz+pXfbGWDuAIA0Q7IC/6I4HEJTiT+fphifYAQN+enGsX9F6njyA6PrVQi8g2AEz+mkCAD6rP1RrXkyeAwydRU9Je2gGAD4a/tkcN6WhYgcM+2ltEh1VOwA/vdLIi6jYAVOmNCDuHQ/AH/0/uzRWbuSFVOyAIT+7NFYQl74BCLFaJ9gB8/bSBAAMe73Rap1gB8xX7eVmzqwBYAHjzX6nMMYOGDYlvS5ph6Q0rQHAo53dl8aauk8GFTtgmPMh3ElLAPBouPvSWNPzdgh2wJ9wL4jxdgDuFd0WCItoO8A/Z9Y+ckxcAgegOeOSNnVfmv+e8AQ7QLgDiHmoE+wA4Q4gRqFOsAMBKhDuAOZWltSb9xjqBDsQfLj3STpGSwCYpihpU77Jy9oIdsCecM9JGhLXuQOQ+vOXxoxeHsvlbkDAnK62p1S7kQ2A1jSuWte78XteULED4Vfvr0nK0xpAyxiWtNNU1zvBDtgZ8HlJBwl4INaKkvbmazew8g3BDljk9NpH8prSS5J6aA0gNspapFc2XRrrD2JlBDtgY8CveSTthPsOSTlaBIikYUm/2nTZ3wqdYAeiGfJ5SVud/03SKoDVYX5C0vCmy/6MoRPsQDyDPuf8dKt22VyalgECN67auPkZSYWgK3OCHYh/4Oecar7+v51O4Oeo8gFPwS3nf6+pdoe4sqRiWBU5wQ5gdvBrdpU/JT05K/zrJwhAlNVDeHronZn1mqIT4N/8t62BTbADMO43ax7Jz/Gr/Dx/9uQCPQb0KLSu2aE6+3fXFqiiZ7z+mYgHMsEOgJMKsycL9aELL8I4SZkvHJtZxjXT63zGkjHnVvP/HSK2Va2TGGsAAAAASUVORK5CYII=',
        queryInfo,
        embedUrlSnippetCreator,
        embedIdSnippetCreator;

      queryInfo = new QueryInfo(
        'get',
        'https://www.googleapis.com/youtube/v3/search',
        function (query) {
          return {
            key: 'AIzaSyB1WyuI9kTh-uyw_u6qSQYDBHehFavwpVc',
            type: 'video',
            maxResults: '8',
            part: 'id,snippet',
            fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails/default,items/snippet/channelTitle',
            q: query
          }
        },
        function (result) {
          return result.data.items.map(function (elm) {
            return new QueryResult(
              Sources.get('youtube'),
              elm.snippet.title,
              elm.snippet.description,
              elm.snippet.thumbnails.default,
              elm.id.videoId
            );
          });
        }
      );

      embedUrlSnippetCreator = function (sourceInfo) {
        var newScope;
        newScope = $rootScope.$new(true);
        newScope.videoUrl = sourceInfo.raw;
        newScope.playerVars = {
          controls: 1,
          autoplay: 1
        };
        return $compile('<youtube-video video-url="videoUrl" player-vars="playerVars"></youtube-video>')(newScope);
      };

      embedIdSnippetCreator = function (queryResult) {
        var newScope;
        newScope = $rootScope.$new(true);
        newScope.videoId = queryResult.videoId;
        newScope.playerVars = {
          autoplay: 1
        };
        return $compile('<youtube-video video-id="videoId" player-vars="playerVars"></youtube-video>')(newScope);
      };

      Sources.hookSource(name, logo, queryInfo, embedUrlSnippetCreator, embedIdSnippetCreator);
    }
    function hookVimeoSource() {
      var name = 'vimeo',
        logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAzoAAADqCAYAAABqZzAGAAAjYklEQVR42u3d2ZHjOtKG4TJBd3XLu7mbkAk0QSbQBJlAD2SCTJAJNEEmyASZ0H/3/KyZOtW1aOGCTDxvBGIi5vRZBCSA/DITyZcXAMXz+q9/N79H+3v0v8fh9xjejcvv8Wv837f/78+f2/0eG7MHAAAAoCRhs/89Tr/HdRQyj44//4ydWQUAAACwprg5Pylsvhp//rmtmQYAAACwhMBpx6zLr4XGQUkbAAAAgLkEzm7G7M0t2R1iBwAAAMBkAqcdGwb8WnkQOwAAAACeFjib3+NYgMAhdgAAAABMInJ2E3RPm03sWCEAAAAA9wicErM4nzYosFoAAAAAbhE52xWbDTwyWqsGAAAA4DuR0xZcqvbVuHivAwAAAOArkdMFEzjvR28FAQAAAHwUOX1gkfNrzELJ6gAAAAD4r8g5Bhc5b2NvNQEAAAD8ETmHJCJHu2kAAAAA4d/kfDUaKwsAAADUK3J2CUXOn9FZXQAAAKBOkbMN2EJa9zUAAAAA3wqdc1KR82cMVhgAAACoT+T0iUUOoQMAAABUKHKa5CKH0AEAAAAqFDoDoQMAAAAgk8jZViBy/oyD1QYAAADqETrHSoROb7UBAACAOkTOphKR82e0VhwAAACoQ+h0FQmdjRUHAAAA6hA6p0pEztlqAwAAAPUInUslQmdvtQEAAIB6hE4tZWuN1QYAAADqEDltJSLnZLUBAAAAQke3NQAAAACETsFjsNIAAAAAoeNtDgAAAABCp+DRW2UAAACA0PHdHAAAAADhhU6TVORcf4+tFQYAAADqFTsZhU5nZQEAAIC6hc7ZuxwAAAAA2YTOIZHIOVpRAAAAAH+Ezo7IAQAAAJBR7FyJHAAAAADZhE5P5AAAAADIJnQ2QbM6e6sHAAAA4Duxsw/2nZydVQMAAABwi9gZAoicP+2wG6sFAAAA4Fahsyn8uzq9VQIAAACQRewMsjgAAAAAphA8h0IETms1AAAAAEwpdtqV3u2cCBwAAAAASwie4wJNBvZK1AAAAAAsLXj+vN/ZjWVt5wmEzR/x1BE3AAAAAEoTP9sx4/NHsPTfjG78c1uzBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1uP1X//e/B7t77H/PfrfY3g3fn0zru/+3GH8+1szCgAAAGANYdOMouT0e1x+EDOPjtP472jMOAAAwF2+2lsQ+uPoPxndxz9nBst1wLtPsgpv4zT+tR0H+u653Y5Zl7mEzU+ip000l7vRDk9f2Okw/vU/Qm/L9v4rrI8f5ugwztOfQ3ljlwIAKhMxbz7v2/14nsEHu3ysvHHnLr/Y+wcX9zz+vRbs67ntZto4j4whquM/HgzHB3/3dfx7d5XZX3tD+eNHQdzZuQCARHfhdvTFDuOdeC3EJzuPvkkneTDPwjdPOI6fjQPB8w+B0xe0mT6OPtBcdhNnwS7ZnfkHBM5nc7SzkwEEcmTfv3M93PHW9Tsn9H2FwFv2e2vGQ9yB/ZP34FrCp2dj5Qmcj5HzXcVzW7rA+bihNoUfVJeZs1tNQvs7TFzyKHgBoCRB8768/iIajw/vnn8lGSqmHjSGpZzwY4VzuwsicD4K022BB9aw4O/fJbG/uYThpebo0rs67t2Hh6dfvQ07vPszO5G5h235fXS+/+R92cd3o+8fA3MM8jivu3fZmdLv08v43yobvpzoPawsdpcaR2J6PSeoerGzsGOeWuyMjs0aYrFLEMCoShDPeE6+b9owdXTuMDpuHPF/isg5ovMX0faQ9rBL4rxex9/B9qb3t/pKxM1XVRaCZ58cHIc1VWjy+Y2YxfnKKdisfHit3bChC7q/h1qzfxPt38NKtneqMfI7RmH7FeZ80GijWMc1W8nRZ7bXWu2n7KQLHlCW4UmUxalG7KwsIGc5jCvL4oR25Mf9vfS8nSNnI951QTwVFmTokt9FJZWYXEehJau27j7cv5bTjZTgKTt7cyVsvj7LajaQ0pzwLtkhnTWysF94Hk8yWw9Ht1ZLnRM3s9lem+yc3BdcYlJ145wVz60TB/U/0XhC+/vAyJGd3BWA3NZkIE2hUZJrhjTbeHmfk0cImoUOslIdoEPhNnisSRBPcFlGiwYegmfNmmDzrrPg/PZwEJUntG+wlfZVeVrqe3kKIyn9vcgQfH5Lds7DlBqunI24dTQF2l9JIvtaqnOYpJY7XIngzJ8tWCKb5oHv9E6r7E3wwNqCvhWBI3Dzo6FEeS/SBt6INUWkmpnm8UjsPWx/Z3P0o8C5JIv4bgOcjZEFTnWdBRcKuHJaOac1nR0lBsqaTIYS7b3IJegc1/Zo8mgOyzgoChfZbQHzk03ghHC+330c2Xwj+z6UxXV2OMtWdIAiHiwdkRNibCq302MhzoNy1K9LYwYO0GpR+8zisnnBPfvwzLEkdpwdxM4cxhK1lGoINM811xh3ldvptQCRoxz183KH2vblqZDzsKlFXJIwNwWwlKj5HIdzm9iZxWD2CSY/Qu35ofINMjw5f12COdjZ4+VczAV9c6m6jjoBsoseiS9X5eBdhe5Zzm1iZzaDOYpaLJYxs0EeTKOz0+r2eDPznDSix+t0uiv0e1ea53BYs49tAnvZOLeJnZovm2vBc904zB/PaCSL9l0Wtr2jCCTnqhThXVFLfSVsP9uBdzhs795gsXPbG7C7RE7GQ6YrdL5FIB4o30gcvWkW2uNHl7JoYCndAJOUSFfVPGemPag71nqjD2o3B2vnvee92YWz6OSiqXmb4c53Osm70+0WcCbOHPB/zEkrGrjOuekNRvxPIsjiKFflrxohBPRr/o9UXgvcpJyrD8OFON+hkEwg7ieaE8GGlZyfytvpF9eUZMW7UBaHUyo45f0hkZPtsafWh/c/iqzETo+cyvlT4jIJ65ZTVXTn5Cj3EJEX2FAJY0S1qyRteUNFK8ZoBOO/Q4xW5BgNMzmV2R55X4m+mG+iiJz5OlAGEjkej2s3/YjdCE4J4BA5SzuRD877hdHfLkYrs9OBUznfOx2dvdZ1vCu8czQl+NsGPB7Xge2R4JRmMcpyHTZTvAEhLssSOhXO15XImacUVSZhXcfb2Vd3+RpnVbdFGXglbItlqytP/7Urb1iO1o0XfK2PVJWGTF+KOjrZ9t5Kb8eInNjNc5TPKl9jN8ZE47CEo32ygVfbtLrL3Fi6VbMYFzmf9sDkZE8yLuyxnMYswUSOIIMyfxUJxvwZQ+m/ebtayeZMVx9c+4NDIme6y5iTve47HfNf9zsdHbKU+RM5xiJ++KuPca0eqXDhG3NfMBVmDAd7rug3UeY/QZdQJfL2O5FjFJ2xZjRlRCrUmRpz1uZX6lQMP5x77Gmlsl8iJ0f50BPVC0QOoc1fNebP6rzqVV9ERxGXvjGnc1OxU3F2WZbn+Lz6Tli1QkeJvM5/RI6xmC/OuS4nJfuqpaYxg3Mjcvp3dtZbuHUdH85KnncSRI67aEHbaZwbsjpq9YOmZJXQGHNE0TgVfzuC5mT1N1FETqVCx95jf2zHeKRU/5EmNx4A3jYOC14A1sOYVIS7GD4vXbPXVn0TxSYrFTrWnv2xnXnut7EaaEheFbS/12BOjKO4lKwIpzFZWeWY4ncxfNjHr9rYrub4cFZmHRsix0jRGetz+2E7/y9i+vFN/faGd/fZRM/ZYVNQZyvvpIw1HRulQZ8LHeWhq7+JcvckeU9K5BhL2V/FGfjr+Nt3Go3dKKhffSOn2EiZDJsxVWSDyPn8PdPobF3MxSpvotw9lQodIof9PWk/h0pL0Tpd6u58TsL5KfcCGS8C82w83YFkzAza55+8ZxJMWEfouHvqFTpEDvtT6XJ3adpcgjFLK//LC+dn5QdQ823m64fHZu+HKDWbVP74c0bHPCwsdNw91QudWvfducJ7up3YdmoqM748U55WYXas4fys0N1qhsvg7dFZe0vp3Bg52+kmVadT44G9UaIYNwf1NiOo7C66vN3XNzSIOSQV/+3EmcBaAiSHpfZuovLtfe21jbN2bJq5bO04haofD9PBulXz/oG4NQxnQkkipxbf4/iIg5/0zdqUQudciThuV9ibGZIfp/c/SEnTnDWB06VnuzkUPQc4fyMCD+wNwyhJ6FRSRTL8VT4ja7Ehku+yn82KezS6z3DVgCB5tIzY0YhAoxHDMEq7oxI9eP7ureyOKJzH/sby+yobCS28T/sE87ideyO9PYyvyblqA4udmiP+l4QPQfe6KhqG8WmEc717JvsZdJqp8iLD3XSeYB6aCu6wrhCfsEnnB00Y1b98XKgxglPDe5AusNCpsSHF8MmD/SZJJKNdYJ2v41xtxxIL3bMMo8J3pNpIfx5c8p7pizcT3uWE8CFf43dEPE5tRNefNnoFHXb6l8BU5KRefiotiB55nFnUvgmcTeXtPg2D0NFG+vo6/3f02tqFYJIAZKhAeYIA+HnKtODN6drkmYMhuNCp4a3O4Q5b3aXZ3NNFCE8/PbB99T0awyjy7FPvP4vI2SojuvO9xGMlj0SO8rVpgr53Rg4ean2XWOxcggudzCL0UVuNWHJ5nEHY/pgFqyhzaxgqDjQfWETkvJvHat+HJS9ZOxbuF55TCuwboy9PfcAoa3vA4EIn64X0sK0GFX/7iYXcXfNXQVclw4g4divcKVk/6rioyEkgdI5P/O7MJWunAH5hdF+9faT85KHIeEUqvQ0sdJqEl1E7wUWd1gZ/eCD8UJtUQscw3E2BM+LFiZwEQqfjk/xdYv664ndy7liD6O28+58coMuUWZxK6i67l8Ak66g21cfJQgnyB8Xc5dF3d4TOUxfdUElHylIc1Jrne7PwXdITOZNmxqorW0u8V1exo0rFZn+LELm+TvwBrOSHYU/o5Hp0G+ywvTz4G9/v9c6enq+E46uzdLxQOsJncjG5/6yBxuv/WslX0W1y4Xsk6+Px3Ur3cuTg0fHB35z5zfA+mF8Y+YwcbvmBuzkjQQlreAdCZ/XRVCzGT086J01l87XYutwzt6Nj45tECzQfec3/fZfF76Wk89mteC9HPlO3D/qFF+9yivELB2cfxyhF57UkJUfnyu2zt5+LK1HYPTiPNTjgxewBnZ3s/1I7YwW2zYEN/XUfbAL6hgdCR1YnRee1JEKnq/zibguwI0Lnf2VTW2djjOYjyee6X2jvZyxZO698nkZ+I7GzD9cvfaz9TjeROq9lq4ndzDAvkT6A2RRgRz4YOmFHneS16lPOt7LLle+khFmx69pnauBo+qPvRY/KR4vzDXeEjqxO6IdmSdKTs5UXBLLLayF2NHC6pxXcievVSxOVWaPJzQL7vheB5xc9I66Tt5NuXoISvdqntMnMouQPQY15ELUMffAO7KiIR/BzZBX3BM1iojJdRHmh8qpsAvFQwFkaVTwO9l6qbryEjlrUHClKddThy4Z6dpTz2wjJI53FiJykpYLDAnv+xLZkc57stNYIfhXrG24JHdHgIkuIaqrBnKv1Z7AIU1eILdXqeO9mnldNCRboXpTwQf1hZrvM+IHgbQHn6LEme0tQOp+qAUGme73EycwSTWuCGfGRsPx0Xi4uZ1G5ErJp3j7NnzlLKtY7dhmuRX9bUwAi8du40N9VJHSka1N1Xgs+5/1Mc9LYzKK7pVxmiSOexUVIkzV/2M44T9nK/C6FnKPnmvZm4jeIofxAQkd2Ie3js+Bla3OWsES6xM9sKXf72VffJ1ryezBpshQEYSzHNHBA48SOppkPQqcOobNj5IvOd+SHpEfzMu88cMbLaCVP6CxXBpJI6AwzzlHHMZURf/bBfeLgWPOShLHSitARKSozBZ6tPGvJwyRYOV9fiD0dON6ETqTMbXKhc3BHl5WZ/cGZrKbLWpJAa9GBRwK8fKET3mEKYsAHh8mn8xKt81JbiD0NFTnezcJzW7PQadlxUd0os2Vzemfn8lnt6JmCkhsDEToxhE7rchZBWjGbszcX1Ud5i3KMKhY6B05nWQ5XwmzO2t/MOdQYbEwomFN1WiN0lpnc6N3X9oXPb2TH6cjRmb+99gPzVoPjfVlpbo8VipzzSnOdQehcZ5qbbG8q9iufmVGd/ac/qhq4u1wVndYCB36L9I+yXuzHwuc3clSumXluRJDyl/uFushe6/yOztZcl3UuJLPDy8pnZlXfy/nw2xtBsDBCp3cOzje5O0YvirR02VBAuzsUYlM1fENnWHF+axM6/YpzfTV/VQQz1rSxbVA7m+SDva85v51TdBVPhaWVMUoJExj+ptB5vdQaSUq4qfeF2FQNb0gaezb/d6FkHqson1ztbU7w97HtRHOQsdva5iUhwYNsIYRO9M2wK3BOexG4VA5lW4hdZW8tfVx5fnVZW2aeG05XOue8mEz4OI9R36Z0zrM6WkonCrL1ESZ47zB1Wb2VAi6QzQnn5Ij65P/ORuI2rMWdmUlKMM8zzEu2DlnNSvuYyMn5kdDdS1KUp4quhUqbBc/mdAvMT7TL/FKQbV0TO9/9ynPbViJySmj12xGL6UuNhhXsisjJm/1P2YQgyd2zizLRoWvTicY4F1PAy7yUjmsbzjfnO0IwI3kwaC7HtGFnT83fNnAgqJthPrK1lT68JCXB3dNGmeijia5+HtuF5ugq0yDjUNocV9LooRTRniFz0Uw8J9k6ZG0WtCciJ1cpVBXfzkmUfdtEmegdR6lqR/S44IXkIuIIFZXNSVg2VNQ3cxJGmy8zzEmm93enhaPhRE7uoFjZH6Ss/DyMNNEbkcpqjXUxRzNo1LyUbOFBNmfW+c3eWvpY0H1jLnPdv6sEh4IHfzr3rG5rCc7DIdpkU5V1Hrh7NqbjWo3fzUle6lFc1ixwVnfWM1O3tYeE4THwXmxnnp8T4RzG726J0GUnPHoUoF1p3iK3kz4vPE86vTw+f1dZBpdN9PafSdrebieek2MiW7vMbD9N8OqJ7QJ7LFsjgpQfCU3id/fRJrw14dVFT9oF5yli1PJUyN7cyObIyD7qeJbkKGQoqxHEWCd4MYrksEHFpd7IJTu/zi+JSSBK24iTfrUhqhGHh4Xn6kg8yziUWNubLKJedNlHghLMYeL52Cazt/1MdhP5jeJ5wXew2ewpc1vpDAHMTcSJP5n0u4w06gPmxWv2g4roXSH7MmPGYVfQuZe1EcGlwDsmevain3g+sr3PaSeen23wqPdx4f3VCtSE8bc79wuHquhNEbwEY7ewXUWNMpXSjvfIARdViywmx7luOPK597Y7dp1GP5lKQ0ssb5ZYKLe0v8aLyPdgCjTOqAJauU8eB+Cbud0lFTlDgfdLhrneTDwnZ0InXRZn9s5qtQgdZWvucWUcK31YKnjXl2YFm4roqJ8L2pPZnPCSHsdn/T5RW+DdEn2uz/b2fOJ6dP7C28ia51uy1tKxvtFSV9laMRUvtabSdzPPT0+BVxG5KKXjWpPMGToVdt6dZXMEPNaoGEj4nmJ40vGL/n7rYI9pRFCJIL1GX4CdyyjlxTSwJx3XvBtJVz5Q/NucRNmLTlR32jtmPN+iO+erlaolFzr9S0KSBC9P0RdhQ2l+OS+Ru6w1K9nTkbNYbQax6ChQ0vc5l0LvlQyCfWtvTxOBH9/hZHDKT4WV4mbqINm+JCTJvt9nWIizyFuqkr69gzfmIZusK9OhsHMu4/dzukLvlPAt0kXfn4/AjwInw767lpg5TWZPcd+A5BejTYaFiK44p/6oW2cu6krRFrQXBxeXCyf6dw0S1KQP9vbjQidJiVqRWZysQiepyNm5Z5QZFKc4xwjUNXDUqVnRjvY28tNzeOWEE+HRa9o9NE8fff/U/sYgYZaGH0VmcQidUL51BrF/yLQg0S+m4wRzsAl+SO9WtqGT6K2Lq9CytWwPwa8FR5m3Cea3s7dvEzpjEKFPFKT5Nba93gTw23SOlEBQUlhRqcHTWZ3g6vtUgA1dRcYdjIWWrWV7n3Ms+C7Zs98qMopDwnK8IZJjR+gUvTYZ7pxLtkXZ13z5BzfKy9rRp+BR3K6QPZilK9ilwPMt2/ucbcF3SfigmSBGdeNSepkaoRNqXbIENnJ92yjRwjQP/PboIq8llFN0XOtlG2aZ12zfzxkKv0uu5pfQCVQC2gX222SoZXOUrVUY9Rzu/M0dxV13FNfhmPObRAkzZUW3lE6Q2Z3N6SJ0ihQ4fYR3OJUInT6RH71Nsia5ytYSOlr7G39v9Aj6WRRXa+kCx6awc61P5qBtCr5D9pwuQofAIXQqFjpDTX50xAXa1RD1HEtZjgkO7KYQuwkdwSAWc5dVJXswfSz8DsnQ1GY3w7z0RMbqb3D2WQTOO7s6EzpFrUemgEbzkpGEtez9F4Z4ySzkRHHDCh0XFgEZumY6yVy3hE6acY78BqeiIE4WoXMWULNQa0VyjmNf/AsjFMUtUegkagbSFmabmYI3l8LvDjZM6BRzR5Z2FhE6uYVOkrLdIu/xFwdylRGq0t5AXG1qKe+SsmNJSwkOhd8dnUvevbpyULNPW3KTMMiYpb30GFC7CqipMTSmeZdT2ocYtxybSebQ+xxRtuhlawdCh9BZ4U6sInuT3K6iC51joj3V1bJ5rg5QBliRI9mvOH9dov3WcwTqjbIlKnueQ+js3F+TjtN4dm5eKmUMSg/ON8kB61B3OjRVzbFIxnyXpgh4zrreRI7AIcC9wY5VShA3ywTHsr2l/hV0LTaJ3n6navNdW7mHdznz2kqGTX5d4XA8J7TTTYH2Kcsgqrl65pzQeaqR0O6lcsZGH33yapttwHXJFKy8VhVESNQ9x7uc+R12h+z9Dk/Gy+pSqI3+IsQFx9aOahI6d2Vt9hGd3hnvi1oqbLqAa/PLuRd7g10cujZ/RRv9uMB8ZX6QfCrURtnmMvN8ZMuChw8E8U7judi+4L2t7Cv0wY6B1miTLGBZVzYn6cVl43PcbxnNjBfXObmt9gXaZyvYoUSwlOyk++w/7+UO4xsTGZu/7WNX+fvoS6C1GgTU82w6YmPFdzk2e4pI7r6SLoa7Au2zJcBlzkopZa30mzZEzfc2sR3Fn063Qd7pJGwidKl5A25sulXTiBGcm4yHczfhBTa4oFa1z84lRFCunU0fM7pDhUKn6u5oP9hD73lAvO6Sr3k+ilx0kFI5Qh2jDXJYexf1eYCguo8DKq2s+n1O5xx2FkSsSlj4vtzzqeIGdpIGdAab09ecvXWoa9P/I7J0T0TynVNz5dA4vyp7n5P1nrg+Gu18zfUh4CoemM9UltYTNynKoLdJ93PzUjtaYupcVbkIvo6/c3tDGULNTs3ARquuWc9eljWMwqW5sSSJwPm7ZfSmEp9pNzZyUpaW5D5JLHL6F/x3kR3aC0XFI10GFXaHuY4Oz1s71JPLrOy66gydI5U4F3sOvHUQezsLRO1/vt+ahP7RdixJO1njfOX7iUWON3SVO7RrXZ7bYHbhYjeKjgwlyDQMQc4Ce8C49Z7bJxI2gsCJz77k5aftC/6x2HubTtcLzo0RzX4TCJ1DgHPAhzCNR0oB2wC2vRnL9/vxv5mwqeR9YnK/90DZuMiWHns2YYgOETpBGxF4x2k8I3h2Bd1prbLk4jKAm4XtYJO8iknJ2jeLb9PrSMO5MUI9mE8gdCJEvTv2b0zgfB3mFj3vMjS7UdAcX+v7vpEStu/9mqugZL1C52jD+caAckYj0oP56E5MkLPAJwiMORoXHEfb2o0O6C2jG/+e/l252SBQKyB8QzavhrfoPTXzvSHsbDbpQ86NQegsV7YR5Cw42AOGYUQTO691ffvOx3tvNAibrdIOawlLggxCR8mGOTYMI9abrmaCM2tbWZXS9dWHQW82Du2E1UhybowwDnlwOz05CwzDMP5y2vt7HfextPFQaRmjdznKlXRResAW1DwbEYRO5KhdH+QsEAAzDGOtt1z9+Dbrs7damk14l3P3habTFoN7swXraUQQOr3zwllgGIbhXRNuvdR8MIvBcW4MQke5gbPAMAyj3GyX7+U8eKmdGFDOWnvOjZFQ6HSEjrPAMAyjsndMRM4Tl5rvp1DVnBsjitBpCR1ngWFUOC7ez1UrcrbUynOXWsOQqhc5W2trBBE6kdvibwkdwzAeed83nn38NR3W8ODFpp3obdGUTdL1d3jOF4k5ETreFf4ZsruGYdx7Fr9+aLv8Wtf3YmofHYWi7l3qkHNTcvavCZotK13oDISOs8AwkgdWd98EJjWRInJQS5SUyOHclF7iGHBvHQu31Z7QcRYYRlJ/o/+peuTVNxCJHDx0uR0YV72PwKz1pKUGm+ClBqVndHaEjrMgyP1hHoybA0wfy9R+2KcXc0bkwDsNIkdGb/VMSMBOYQNnnNAxnj8LOKPGjcGx9oF96oPvRA4e2Di+qVNpOz8NKZ4e+0TRt8FZVbXQcRY82SGLM2o88w5HJQ6Rg/kuOIdypT3LdXJ5yl52N8xvT+hMaq8doUPoFHgWdJxR4wY72U+0Vzevvq1D5MAlR+TctO4+HPtY04HtHRfSldCZzF43hM5sc8sxfyw6v/3GVpWwGTc1Gnhgv26VnucKjkBWh8ix7iWM070XVqCszhDEZo+EjqBHiQ1InK/G3AIneobbqNvnLOGiu4jMV7nuDp87a/AfyEJcCZ3J7LUhdAQ9opwFWgLrpCbwY7zzOZsXuOgWNLiNVdeM4sYITFtBtHwIZLORLvdtoHm132c4C7x/qsY2jms4su7wnNUgmG/D1HAgDwxO+nvpwynA49FrIJuNlNVpnf913xsejytRm3nfsq+E1SCQ1XnqOwf465B0GM3UIefdPBf/eDSY3faEzuRz6p3OA23ks+x/I5bAIXbyV4NAGpSqrrMUKOzbrdKzZwHtNsLF3smUOQuInVSd9rpC9y+xo3IId1x212SqurOyP17Aoi8LiOHC2/g2Ae32KsCifC3KuhE7ocuY2wD7l9hJVg0CJQwPf+cA1WTybo2+NAvOdakZtDag3e4Kt61DsPnsRGLnvTOInVBO6yFgAGijQUF+PwL1lIVIHSpbCZntK1Ts7ILabsnO+RBwPi/OgkWykSLv5ZYsdtH9ByXpsji4zfGNGnXqreBDa17TNx8Oa19kBc53H9h2SxU7F1kyZ8E3kXelgmVlb7bJ7vRO9nD2kkZZnASbJFqpWmvlqs7khTqYRqeylIvoGNx2Sy0J2gScy1o+NdAUMNcHDuOq98Eu+Z0uezhP1o+vmWiTHAMdWErV8jqLUzg1baFz3hTiWA4J7LfE+vQ24Dw2iSPBxZ0F46cdLhxIpWkznosEdeKue3h+g5wLTznvrNTkWQZOzToZ1DWdy2syGy7FUe+DzmFL4HBGE4mbfe1lRgQ1gYN4YucoizOr060d6Dp7rV/RSW+SnVt9AYJncA4QOHc6o97uTHMHdN5QfGpj/au3OwQOPnUaLgVFZ1qrwsnJ0A70Byd96T3XJT27+jXPL+fAKmfBMfJZMM676Pt9TulxzOYKgMogPhscIXBkdnz8s5I13wa4bFPXXI+/bal3J8fk9rxb6d3hzjngLHhi/3tM/rk/cBpL0nwv73H7arSizhEcwbRiZ1jBAHtRmlXX/FigQ1NVzfW4Dm+iZ66yg0tFc/kmei4EZNhz4C2Sf8h+FowlbUfChrCZcX/3FWYRTwLo+GpT9AtdYHsCp6iLdlg5nbwXcflHlH0/HtRTXk7bCueyGUXkYSYbv2Y5x8Zz4FRAacm+UlvdLJzlXTOYdRh/K2GzrI3tktvXqbbOe3jO0RpmMkKd1Mpe9+MCjxmHUVCrub7PCe3GeTuOc3hWvvaw+GlHh7ofz6XhiTOvSzg/h4UiwMP473IWfC56ljiP58zUDO9ETWtlieqZsr78Sjy8GboJBA+FHdex7sf1vz7hxJzGf47o3TKXV/vNMP+Pif/2h9Ek//370eE+PyloBmfBU+swZ2byWbH6/pxv3ffh7Gs32tY5UDawsXKYOsK3v8HpfX/gid7kFD/VOn0AbhLUzoLl7uUpM5MfMzFv4zj+89+y8IImddz1+3fVA2tkat4HRviTAAAAAGYNcOzeCd/jB1H8mdC+fPFnPhPQgiMT8H8UgoCIWa8aYgAAAABJRU5ErkJggg==',
        queryInfo,
        embedUrlSnippetCreator,
        embedIdSnippetCreator;

      queryInfo = new QueryInfo(
        'get',
        'https://api.vimeo.com/videos',
        function (query) {
          return {
            query: query,
            per_page: '8',
            access_token: '12d626cfe1113cdbc071a1cdf0e80176'
          }
        },
        function (result) {
          return result.data.data.map(function (elm) {
            elm.pictures.sizes[0].url = elm.pictures.sizes[0].link;
            return new QueryResult(
              Sources.get('vimeo'),
              elm.name,
              elm.description,
              elm.pictures.sizes[0],
              elm.uri.slice(elm.uri.lastIndexOf('/') + 1)
            );
          });
        }
      );

      embedUrlSnippetCreator = function (sourceInfo) {
        var newScope;
        newScope = $rootScope.$new(true);
        newScope.videoUrl = sourceInfo.raw;
        newScope.playerVars = {
          autoplay: 1
        };
        return $compile('<vimeo-video video-url="videoUrl" player-opts="playerVars" ></vimeo-video>')(newScope)
      };

      embedIdSnippetCreator = function (queryResult) {
        var newScope;
        newScope = $rootScope.$new(true);
        newScope.videoId = queryResult.videoId;
        newScope.playerVars = {
          autoplay: 1
        };
        return $compile('<vimeo-video video-id="videoId" player-opts="playerVars" ></vimeo-video>')(newScope)
      };

      Sources.hookSource(name, logo, queryInfo, embedUrlSnippetCreator, embedIdSnippetCreator);
    }

    function hookSource(name) {
      if (name === 'youtube') {
        hookYoutubeSource();
      }
      if (name === 'vimeo') {
        hookVimeoSource();
      }
    }

    function unhookSource(name) {
      Sources.remove(name);
    }

    hookSource('youtube');
    // hookSource('vimeo');

    $scope.sourcesModel = {
      youtube: true,
      vimeo: false,
      asyncSelected: '',
      url: ''
    };
    /* ---------------------------------------------------- */

    $scope.onSourceHookChange = function (name, hook) {
      if (hook) {
        hookSource(name);
      } else {
        unhookSource(name);
      }

      $scope.sourceInfo = null;
      $scope.selectedQueryResult = null;

      $scope.sourcesModel.asyncSelected = '';
      $scope.sourcesModel.url = '';
    };

  });

angular.module('playbuzzAppApp')
  .directive('sourceUrlEmbed', function () {
    return {
      restrict: 'E',
      scope: {
        sourceInfo: '='
      },
      template: '<div id="container" class="source-url-embed"></div>',
      link: function ($scope, $element) {
        $scope.$watch(function () {
          return $scope.sourceInfo;
        }, function (nValue) {
          if (!!nValue) {
            $element.find('#container').empty();
            if (!!$scope.sourceInfo.source) {
              $element.find('#container').append($scope.sourceInfo.source.embedUrlSnippetCreator.apply(this, [$scope.sourceInfo]));
            } else {
              $element.find('#container').append('We\'re sorry but [' + $scope.sourceInfo.raw + '] is not supported source');
            }
          }
        })
      }
    };
  })
  .directive('sourceIdEmbed', function () {
    return {
      restrict: 'E',
      scope: {
        queryResult: '='
      },
      template: '<div id="container" class="source-id-embed"></div>',
      link: function ($scope, $element) {
        $scope.$watch(function () {
          return $scope.queryResult;
        }, function (nValue) {
          if (!!nValue) {
            $element.find('#container').empty();
            $element.find('#container').append($scope.queryResult.source.embedIdSnippetCreator.apply(this, [$scope.queryResult]));
          }
        })
      }
    };
  });
