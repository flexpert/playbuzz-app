'use strict';

function Source(name, logo, queryInfo, embedUrlSnippetCreator, embedIdSnippetCreator) {
  var self = this;
  self.name = name;
  self.logo = logo;
  self.queryInfo = queryInfo;
  self.embedUrlSnippetCreator = embedUrlSnippetCreator;
  self.embedIdSnippetCreator = embedIdSnippetCreator;
}

function QueryInfo(method, url, configObjCreator, handler) {
  var self = this;
  self.method = method;
  self.url = url;
  self.configObjCreator = configObjCreator;
  self.handler = handler;
}

function QueryResult(source, title, description, thumbnail, videoId) {
  var self = this;
  self.source = source;
  self.title = title;
  self.description = description;
  self.thumbnail = thumbnail;
  self.videoId = videoId;
}

angular.module('playbuzzAppApp')
  .service('Sources', function ($q, $http) {
    var self = this,
      sources = [];

    function concatArrays(arrays) {
      return arrays.reduce(function (previousValue, currentValue) {
        return previousValue.concat(currentValue);
      }, []).sort();
    }

    self.hookSource = function (name, logo, queryInfo, embedUrlSnippetCreator, embedIdSnippetCreator) {
      sources.push(new Source(name, logo, queryInfo, embedUrlSnippetCreator, embedIdSnippetCreator));
    };

    self.querySources = function (query) {
      return $q.all(sources.map(function (source) {
          var method = source.queryInfo.method,
            url = source.queryInfo.url,
            params = source.queryInfo.configObjCreator.apply(this, [query]),
            handler = source.queryInfo.handler;

          return $http({
            method: method,
            url: url,
            params: params
          }).then(handler)
        })
      ).then(concatArrays);
    };

    self.parseSourceUrl = function (url) {
      var parser = document.createElement('a'),
        parsed = {},
        parsedUrl = {};
      parser.href = url || '';

      parsedUrl.hostname = parser.hostname.split('.').join('');
      parsedUrl.path = parser.pathname;
      parsedUrl.search = {};
      parser.search.slice(1).split('&').reduce(function (previousValue, currentValue) {
        var splitted = currentValue.split('=');
        previousValue[splitted[0]] = splitted[1];
        return previousValue;
      }, parsedUrl.search);

      parsed.source = sources.find(function (elm) {
        return parsedUrl.hostname.search(elm.name) > -1;
      });

      parsed.urlInformation = parsedUrl;
      parsed.raw = url;

      return parsed;
    };

    self.get = function (name) {
      return sources.find(function (elm) {
        return name.search(elm.name) > -1;
      });
    };

    self.remove = function (name) {
      var source = sources.find(function (elm) {
        return name.search(elm.name) > -1;
      });

      sources.splice(sources.indexOf(source),1);
    };

  });
