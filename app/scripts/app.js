'use strict';

/**
 * @ngdoc overview
 * @name playbuzzAppApp
 * @description
 * # playbuzzAppApp
 *
 * Main module of the application.
 */
angular
  .module('playbuzzAppApp', [
    'ui.bootstrap',
    'youtube-embed',
    'vimeoEmbed'
  ]);
